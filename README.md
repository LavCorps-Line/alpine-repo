LavCorps-Line Alpine Linux Repository
=====================================

This repository contains custom APKBUILD files for Alpine Linux. While the
intent is to one day have them merged to aports, they remain here until they
can be considered polished enough to even think of adding them.

Git Hooks
---------

You can find some useful git hooks in the `.githooks` directory.
To use them, run the following command after cloning this repository:

```sh
git config --local core.hooksPath .githooks
```

Repository Access
-----------------

To access the repository in apk, run the following commands:

```sh
curl "https://s3.lavcorps.xyz/alpine/v3.19/x86_64/lavcorps@protonmail.com.rsa.pub" | tee /etc/apk/keys/lavcorps@protonmail.com.rsa.pub
echo "@lavcorps https://s3.lavcorps.xyz/alpine/v3.19" | tee -a /etc/apk/repositories
```